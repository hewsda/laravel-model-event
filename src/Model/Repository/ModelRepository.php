<?php

declare(strict_types=1);

namespace Hewsda\LaravelModelEvent\Model\Repository;

use Hewsda\LaravelModelEvent\ModelRoot;
use Prooph\ServiceBus\EventBus;

abstract class ModelRepository
{
    /**
     * @var EventBus
     */
    protected $eventBus;

    /**
     * AggregateRepository constructor.
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    protected function saveAggregateRoot(ModelRoot $root): void
    {
        $events = $root->popRecordedEvents();

        foreach ($events as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}