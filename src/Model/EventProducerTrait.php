<?php

declare(strict_types=1);

namespace Hewsda\LaravelModelEvent\Model;

use Hewsda\LaravelModelEvent\ModelChanged;

trait EventProducerTrait
{
    /**
     * @var int
     */
    protected $version = 0;

    /**
     * @var array
     */
    protected $recordedEvents = [];

    public function popRecordedEvents(): array
    {
        $pendingEvents = $this->recordedEvents;

        $this->recordedEvents = [];

        return $pendingEvents;
    }

    protected function recordThat(ModelChanged $event): void
    {
        ++$this->version;

        $this->recordedEvents[] = $event->withVersion($this->version);

        $this->apply($event);
    }

    abstract protected function aggregateId(): string;

    abstract public function apply(ModelChanged $event): void;
}