<?php

declare(strict_types=1);

namespace Hewsda\LaravelModelEvent;

use Hewsda\LaravelModelEvent\Model\EventProducerTrait;
use Hewsda\LaravelModelEvent\Model\Timestampable;
use Illuminate\Database\Eloquent\Model;

abstract class ModelRoot extends Model
{
    use EventProducerTrait, Timestampable;

    public function apply(ModelChanged $event): void
    {
        $handler = $this->determineEventHandlerMethodFor($event);
        if (!method_exists($this, $handler)) {
            throw new \RuntimeException(sprintf(
                'Missing event handler method %s for aggregate root %s',
                $handler,
                get_class($this)
            ));
        }

        $this->{$handler}($event);
    }

    protected function determineEventHandlerMethodFor(ModelChanged $event): string
    {
        return 'when' . implode(array_slice(explode('\\', get_class($event)), -1));
    }
}